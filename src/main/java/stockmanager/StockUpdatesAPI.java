package stockmanager;

import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.lettuce.core.pubsub.api.reactive.RedisPubSubReactiveCommands;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.reactivex.CompletableHelper;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by v.dominte on 12/14/18.
 */
public class StockUpdatesAPI extends LettuceCacheService {

    public static final String STOCK_UPDATES_ADDRESS = "stock-updates";

    private final Logger LOGGER = Logger.getLogger(StockUpdatesAPI.class.getName());

    @Override
    protected void initSuccess(Future<Void> startFuture) {
        LOGGER.info("Starting StockUpdatesAPI");

        final StatefulRedisConnection<String, String> connection = connections.get(CONNECTION);
        final StatefulRedisPubSubConnection<String, String> pubSubConnection =
                (StatefulRedisPubSubConnection<String, String>) connections.get(PUB_SUB_CONNECTION);
        RedisCommands<String, String> syncCommands = connection.sync();
        RedisPubSubReactiveCommands<String, String> pubSubReactiveCommands = pubSubConnection.reactive();
        pubSubReactiveCommands.subscribe("__keyevent@0__:incrby").subscribe();
        pubSubReactiveCommands.observeChannels().subscribe(message -> {
            getLogger().info(String.format("Got %s on channel %s", message.getMessage(), message.getChannel()));
            vertx.<String>executeBlocking(fut -> {
                final String updatedQuantity = syncCommands.get(message.getMessage());
                fut.complete(updatedQuantity);
            }, res -> {
                final String result = res.result();
                getLogger().info(String.format("Updated quantity for product code %s is %s",
                        message.getMessage(),
                        result));
                JsonObject json = new JsonObject()
                        .put("productCode", message.getMessage())
                        .put("quantity", result);
                vertx.eventBus().publish(STOCK_UPDATES_ADDRESS, json.encode());
            });
        });

        Router router = Router.router(vertx);
        router.get("/").handler(rc -> {
            rc.response().putHeader("content-type", "text/html")
                    .end("Welcome to Stock Updates API Service");
        });
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx);
        BridgeOptions options = new BridgeOptions();
        options.addOutboundPermitted(new PermittedOptions().setAddress(STOCK_UPDATES_ADDRESS));
        sockJSHandler.bridge(options);
        router.route("/eventbus/*").handler(sockJSHandler);
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .rxListen(config().getInteger("http.port", 8080))
                .doOnSuccess(server -> LOGGER.info("HTTP server started"))
                .doOnError(t -> LOGGER.log(Level.SEVERE, "HTTP server failed to start", t))
                .toCompletable()
                .subscribe(CompletableHelper.toObserver(startFuture));
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
