import React from 'react'
import ReactDOM from 'react-dom'
import StockUpdates from './components/StockUpdates'
ReactDOM.render(<StockUpdates />, document.getElementById('app'))
