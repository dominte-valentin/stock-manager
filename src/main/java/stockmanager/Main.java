package stockmanager;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

/**
 * Created by v.dominte on 11/3/18.
 */
public class Main {

    public static void main(String[] args) {
        Vertx vertx = Vertx.vertx();
        DeploymentOptions restAPIOptions = new DeploymentOptions()
                .setConfig(new JsonObject()
                        .put("http.port", 8081)
                        .put("cache.host", "localhost")
                        .put("cache.port", 6379)
                );
        DeploymentOptions pushAPIOptions = new DeploymentOptions()
                .setConfig(new JsonObject()
                        .put("http.port", 8082)
                        .put("cache.host", "localhost")
                        .put("cache.port", 6379)
                );
        vertx.deployVerticle(StockRestAPI.class.getName(), restAPIOptions);
        vertx.deployVerticle(StockUpdatesAPI.class.getName(), pushAPIOptions);
    }
}
