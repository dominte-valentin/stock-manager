Config Redis
------------
- docker pull redis
- docker run --name=redis --detach=true --publish=6379:6379 redis
- docker exec -it redis redis-cli
- config set notify-keyspace-events KEA
- psubscribe '\_\_key*__:*'

Start the Server
----------------
- ./gradlew build
- ./gradlew run

Postman Collection
------------------
- https://www.getpostman.com/collections/7542e06afedeefdd92b4

Start the client
----------------
- cd client-app
- npm install
- npm start
- http://localhost:9000/