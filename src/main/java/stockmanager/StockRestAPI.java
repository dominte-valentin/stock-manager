package stockmanager;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by v.dominte on 11/3/18.
 */
public class StockRestAPI extends CacheService {

    public static final String API_ENDPOINT = "/api";
    public static final String PRODUCTS_ENDPOINT = "/api/products";

    private final Logger LOGGER = Logger.getLogger(StockRestAPI.class.getName());

    @Override
    protected void initSuccess(Future<Void> startFuture) {
        Router router = Router.router(vertx);

        router.get("/").handler(rc -> {
            rc.response().putHeader("content-type", "text/html")
                    .end("Welcome to Stock Manager API Service");
        });
        router.get(API_ENDPOINT).handler(rc -> rc.response()
                .putHeader("content-type", "application/json")
                .end(new JsonObject()
                        .put("name", "StockManager")
                        .put("version", 1)
                        .encode()));
        router.route().handler(BodyHandler.create());
        router.post(PRODUCTS_ENDPOINT).handler(this::handleAddProduct);
        router.get(PRODUCTS_ENDPOINT + "/:code").handler(this::handleGetByCode);
        vertx.createHttpServer() // creates a HttpServer
                .requestHandler(router::accept) // router::accept will handle the requests
                .listen(
                        config().getInteger("http.port", 8080), // Get "http.port" from the config, default value 8080
                        ar -> {
                            if (ar.succeeded()) {
                                startFuture.complete();
                            } else {
                                startFuture.fail(ar.cause());
                            }
                        });
    }

    private void handleGetByCode(RoutingContext rc) {
        String code = rc.request().getParam("code");
        LOGGER.info(String.format("Getting product quantity for code %s", code));
        vertx.<String>executeBlocking(fut -> {
            final String quantity = cache.get(code);
            if (quantity != null) {
                fut.complete(quantity);
            } else {
                fut.fail(String.format("Product with code %s not found!", code));
            }
        }, res -> {
            String response;
            if (res.succeeded()) {
                String quantity = res.result();
                LOGGER.info(String.format("Product with code %s has quantity %s", code, quantity));
                rc.response().putHeader("content-type", "application/json");
                response = new JsonObject()
                        .put("code", code)
                        .put("quantity", quantity)
                        .encode();
            } else {
                response = res.cause().getMessage();
                LOGGER.info(response);
                rc.response().setStatusCode(HttpResponseStatus.NOT_FOUND.code());
            }
            rc.response().end(response);
        });
    }

    private void handleAddProduct(RoutingContext rc) {
        LOGGER.info("Adding Product");
        HttpServerResponse response = rc.response();
        JsonObject bodyAsJson = rc.getBodyAsJson();
        if (bodyAsJson != null && bodyAsJson.containsKey("code") && bodyAsJson.containsKey("quantity")) {
            vertx.<Long>executeBlocking(fut -> {
                final Long qty = cache.incrBy(bodyAsJson.getString("code"), bodyAsJson.getInteger("quantity"));
                fut.complete(qty);
            }, res -> {
                if (res.succeeded()) {
                    LOGGER.info(String.format("Product added. Current quantity is %s", res.result()));
                    response.setStatusCode(HttpResponseStatus.CREATED.code())
                            .end("Product added.");
                } else {
                    LOGGER.log(Level.SEVERE, "Adding product failed!", res.cause());
                    response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code()).end("Adding product failed!");
                }
            });
        } else {
            LOGGER.info("Add product request is invalid!");
            response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code())
                    .end(String.format("Body is %s. 'code' and 'quantity' should be provided", bodyAsJson));
        }
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
