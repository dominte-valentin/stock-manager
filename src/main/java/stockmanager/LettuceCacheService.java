package stockmanager;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import io.vertx.core.Future;
import io.vertx.reactivex.core.AbstractVerticle;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by v.dominte on 11/22/18.
 */
public abstract class LettuceCacheService extends AbstractVerticle {

    public static final String CONNECTION = "CONNECTION";
    public static final String PUB_SUB_CONNECTION = "PUB_SUB_CONNECTION";
    protected Map<String, StatefulRedisConnection<String, String>> connections;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.<Map<String, StatefulRedisConnection<String, String>>>executeBlocking(fut -> {
            final String host = config().getString("cache.host", "localhost");
            final Integer port = config().getInteger("cache.port", 12001);
            RedisClient redisClient = RedisClient
                    .create("redis://" + host + ":" + port + "/");
            StatefulRedisConnection<String, String> connection = redisClient.connect();
            StatefulRedisPubSubConnection<String, String> pubSubConnection = redisClient.connectPubSub();
            Map<String, StatefulRedisConnection<String, String>> connections = new HashMap<>();
            connections.put(CONNECTION, connection);
            connections.put(PUB_SUB_CONNECTION, pubSubConnection);
            fut.complete(connections);
        }, res -> {
            if (res.succeeded()) {
                getLogger().info("Lettuce Cache connections successfully done");
                connections = res.result();
                initSuccess(startFuture);
            } else {
                getLogger().log(Level.SEVERE, "Lettuce Cache connections failed", res.cause());
                startFuture.fail(res.cause());
            }
        });
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        if (connections != null) {
            vertx.executeBlocking(fut -> {
                for (StatefulRedisConnection connection: connections.values()) {
                    connection.close();
                }
                fut.complete();
            }, res -> {
                stopFuture.complete();
            });
        } else {
            stopFuture.complete();
        }
    }

    protected abstract void initSuccess(Future<Void> startFuture);

    protected abstract Logger getLogger();
}
