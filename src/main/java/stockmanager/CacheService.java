package stockmanager;

import io.vertx.core.Future;
import io.vertx.reactivex.core.AbstractVerticle;
import redis.clients.jedis.Jedis;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by v.dominte on 11/22/18.
 */
public abstract class CacheService extends AbstractVerticle {
    protected Jedis cache;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.<Jedis>executeBlocking(fut -> {
            final String host = config().getString("cache.host", "localhost");
            final Integer port = config().getInteger("cache.port", 12001);
            Jedis result = new Jedis(host, port);
            fut.complete(result);
        }, res -> {
            if (res.succeeded()) {
                getLogger().info("Jedis Cache connection successfully done");
                cache = res.result();
                initSuccess(startFuture);
            } else {
                getLogger().log(Level.SEVERE, "Jedis Cache connection failed", res.cause());
                startFuture.fail(res.cause());
            }
        });
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        if (cache != null) {
            vertx.executeBlocking(fut -> {
                cache.quit();
                fut.complete();
            }, res -> {
                stopFuture.complete();
            });
        } else {
            stopFuture.complete();
        }
    }

    protected abstract void initSuccess(Future<Void> startFuture);

    protected abstract Logger getLogger();
}
