import React from 'react'
import EventBus from 'vertx3-eventbus-client'

class StockUpdates extends React.Component {

    constructor(props) {
        super(props);
        const eventBus = new EventBus("http://localhost:8082/eventbus");
        var _this = this;
        eventBus.enableReconnect(true);
        eventBus.onopen = function () {
            eventBus.registerHandler('stock-updates', function (error, message) {
                if (error === null) {
                    console.info(message.body);
                    var stockUpdate = JSON.parse(message.body);
                    _this.setState({productCode:stockUpdate.productCode});
                    _this.setState({quantity:stockUpdate.quantity});

                } else {
                    console.error(error, 'stock-updates');
                }
            });
        };
        this.state = {
            productCode: '',
            quantity: ''
        };
    }

    render() {
        return (
            <div className='cute'>
                <h1>Last stock update</h1>
                <p>For product with code [{this.state.productCode}] updated quantity is [{this.state.quantity}]</p>
            </div>
        );
    }
}

export default StockUpdates
