package stockmanager;

import java.io.IOException;
import java.net.ServerSocket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;
import redis.clients.jedis.Jedis;

import static stockmanager.StockRestAPI.API_ENDPOINT;
import static stockmanager.StockRestAPI.PRODUCTS_ENDPOINT;

/**
 * Created by v.dominte on 11/23/18.
 */
@RunWith(VertxUnitRunner.class)
public class StockRestAPITest {

    private Vertx vertx;
    private int port;
    private String host;
    private WebClient webClient;
    private Jedis cache;

    @Before
    public void setUp(TestContext context) throws IOException {
        vertx = Vertx.vertx();
        ServerSocket socket = new ServerSocket(0);
        host = "localhost";
        port = socket.getLocalPort();
        socket.close();
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(new JsonObject()
                        .put("http.port", port)
                        .put("cache.host", host)
                        .put("cache.port", 6379)
                );
        webClient = WebClient.create(vertx);
        vertx.deployVerticle(StockRestAPI.class.getName(), options, context.asyncAssertSuccess());
        cache = new Jedis(host, 6379);
        cache.set("42","100");
    }

    @After
    public void cleanUp(TestContext context) {
        cache.quit();
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void test_welcome_endpoint(TestContext context) {
        final Async async = context.async();
        webClient.get(port, host, "/").send(context.asyncAssertSuccess(response -> {
            String body = response.body().toString("ISO-8859-1");
            context.assertTrue(body.contains("Welcome"));
            context.assertEquals(200, response.statusCode());
            context.assertEquals("text/html", response.headers().get("content-type"));
            async.complete();
        }));
    }

    @Test
    public void non_existing_endpoint(TestContext context) {
        final Async async = context.async();

        webClient.get(port, host, "/nothing").send(context.asyncAssertSuccess(response -> {
            context.assertEquals(404, response.statusCode());
            async.complete();
        }));
    }

    @Test
    public void api_endpoint(TestContext context) {
        final Async async = context.async();

        webClient.get(port, host, API_ENDPOINT).send(context.asyncAssertSuccess(response -> {
            context.assertEquals(200, response.statusCode());
            context.assertEquals("application/json", response.headers().get("content-type"));
            context.assertEquals("{\"name\":\"StockManager\",\"version\":1}", response.body().toString());
            async.complete();
        }));
    }

    @Test
    public void post_products_endpoint(TestContext context) {
        final Async async = context.async();
        JsonObject body = new JsonObject().put("code", "123").put("quantity", 1);
        webClient.post(port, host, PRODUCTS_ENDPOINT).sendJsonObject(body, context.asyncAssertSuccess(response -> {
            context.assertEquals(201, response.statusCode());
            context.assertEquals("Product added.", response.body().toString());
            async.complete();
        }));
    }

    @Test
    public void post_products_with_bad_format(TestContext context) {
        final Async async = context.async();
        JsonObject body = new JsonObject().put("code", "123").put("quantity", "1");
        webClient.post(port, host, PRODUCTS_ENDPOINT).sendJsonObject(body, context.asyncAssertSuccess(response -> {
            context.assertEquals(500, response.statusCode());
            async.complete();
        }));
    }

    @Test
    public void post_products_without_code(TestContext context) {
        final Async async = context.async();
        JsonObject body = new JsonObject().put("quantity", 1);
        webClient.post(port, host, PRODUCTS_ENDPOINT).sendJsonObject(body, context.asyncAssertSuccess(response -> {
            context.assertEquals(400, response.statusCode());
            context.assertEquals("Body is {\"quantity\":1}. 'code' and 'quantity' should be provided", response.body().toString());
            async.complete();
        }));
    }

    @Test
    public void post_products_without_quantity(TestContext context) {
        final Async async = context.async();
        JsonObject body = new JsonObject().put("code", "c");
        webClient.post(port, host, PRODUCTS_ENDPOINT).sendJsonObject(body, context.asyncAssertSuccess(response -> {
            context.assertEquals(400, response.statusCode());
            context.assertEquals("Body is {\"code\":\"c\"}. 'code' and 'quantity' should be provided", response.body().toString());
            async.complete();
        }));
    }

    @Test
    public void get_products_with_code(TestContext context) {
        final Async async = context.async();
        webClient.get(port, host, PRODUCTS_ENDPOINT + "/42").send(context.asyncAssertSuccess(response -> {
            context.assertEquals(200, response.statusCode());
            context.assertEquals("{\"code\":\"42\",\"quantity\":\"100\"}", response.body().toString());
            async.complete();
        }));
    }

    @Test
    public void get_products_with_unexisting_code(TestContext context) {
        final Async async = context.async();
        webClient.get(port, host, PRODUCTS_ENDPOINT + "/43").send(context.asyncAssertSuccess(response -> {
            context.assertEquals(404, response.statusCode());
            context.assertEquals("Product with code 43 not found!", response.body().toString());
            async.complete();
        }));
    }
}
